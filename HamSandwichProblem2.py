### Inspired by Numberphile video: https://www.youtube.com/watch?v=YCXmUi56rao
# See end of video for a follow-up question: cut the below in half with one cut:
#   oo
#   ooo
# (but he two lines of text are touching)

import numpy as np
import matplotlib.pyplot as plt


### Methods:
# 1. Generate random points on disks and find center of mass.
# 2. Generate random points on rings and find center of mass.
# 3. Just use the five origins and find center of mass.


### To define Circles parameters
class Circle():
  def __init__(self, x0, y0, radius):
    self.x0 = x0
    self.y0 = y0
    self.O = (x0, y0)
    self.radius = radius


### Fill circle/distk uniformly with n points
def Fill(x0, y0, R=1, n=1000):
  # x0, y0 = float
  # R = float
  # n = int
  r = np.random.uniform(low=0., high=R, size=n)             # radius of point
  theta = np.random.uniform(low=0., high=2.*np.pi, size=n)  # angle of point

  # Location of points in rectangular coords.
  x = x0 + np.sqrt(r)*np.cos(theta)
  y = y0 + np.sqrt(r)*np.sin(theta)
  return x, y




### Make Circle objects. Make in shape of
#   ooo
#   ooo
# then del top-right.
# Centers:
#   45
#   123
# I'm sure this could be simply cast/vectorized somehow...
R = 1
Circles = []
shape = (3,2) # (x,y)
# 1, 3
for y in range(shape[1]):
  Y = R * (2*y + 1)
  # 1, 3, (5)
  for x in range(shape[0]):
    X = R * (2*x + 1)
    Circles.append( Circle(X,Y,R) )

del Circles[-1]




### Fill and plot Circles: points and borders
# Number of randomly-generated points
n = 1000

# Lists to hold all randomly-generated points
X = []
Y = []

plt.figure(figsize=(3*R*shape[0], 3*R*shape[1]))
for i in range(len(Circles)):

  # Plot Circle border
  # https://stackoverflow.com/questions/9215658/plot-a-circle-with-pyplot/23498706
  ax = plt.gca() # get current axes
  circle = plt.Circle(Circles[i].O, Circles[i].radius, fill=False)
  ax.add_artist(circle)

  # Fill Circle with points
  x,y = Fill(Circles[i].x0, Circles[i].y0, R, n)
  X.append(x)
  Y.append(y)
  # Create scatter in Circle
  plt.scatter(x, y, s=3)#, label='Circle '+str(i+1))
  #plt.text(Circles[i].x0, Circles[i].y0, str(i+1))

#plt.legend()
#plt.show(block=False)






# Flatten X and Y, create ones array, concat to create [[1,x0], [1,x1], ...]
X2 = np.reshape(np.array(X),(-1,1))
ones = np.ones((len(X2),1))
X3 = np.concatenate( (ones,X2), axis=1 )
Y2 = np.reshape(np.array(Y),(-1,1))

# Normal Equation to find theta, i.e. b,m
# y = b + m*x
theta = np.linalg.inv(X3.T.dot(X3)).dot(X3.T).dot(Y2)
b, m = theta
linestring = 'y = '+str(round(m[0],2))+'x + '+str(round(b[0],2))

# Plot this line with some given values of x,y
minx = 0
maxx = max([i.x0 for i in Circles])+R
linex = [minx, maxx]
liney = [np.dot((1,i),theta)[0] for i in linex]
plt.plot(linex, liney, label=linestring)

# Center of mass of randomly-generated points
Xcm = np.mean(X2)
Ycm = np.mean(Y2)
comstring = 'CoM: ('+str(round(np.mean(X2),2))+', '+str(round(np.mean(Y2),2))+')'

# Plot center of mass of randomly-generated points
plt.scatter(Xcm, Ycm, color='k', label=comstring)
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.savefig('Fit.png')
plt.show(block=False)






### What side of line are points on?
# https://math.stackexchange.com/questions/274712/calculate-on-which-side-of-a-straight-line-is-a-given-point-located
# "To determine which side of the line from A=(x1,y1) to B=(x2,y2) a point P=(x,y) falls on"
def SideOfLine(A,B,P):
  x1,y1 = A
  x2,y2 = B
  x,y = P
  # (x[0]-linex[0])*(liney[1]-liney[0])-(y[0]-liney[1])*(linex[1]-linex[0])
  return (x-x1)*(y2-y1) - (y-y1)*(x2-x1)
  
A = (minx, np.dot((1,minx),theta)[0])
B = (maxx, np.dot((1,maxx),theta)[0])
'''
d = np.array( [SideOfLine( A, B, (x,y)) for x,y in zip(X2,Y2)])
# "If d<0 then the point lies on one side of the line, 
# and if d>0 then it lies on the other side. 
# If d=0 then the point lies exactly line."
# (0,0) is positive:
# np.sign((0-linex[0])*(liney[1]-liney[0])-(0-liney[1])*(linex[1]-linex[0])) = 1.0
dp = d[d >= 0]
dn = d[d < 0 ]
print('dp =',len(dn),'; dn =',len(dn), '; dp/dn=', len(dp)/len(dn))
'''

### Do the above in a loop. Save point respectively
Side1 = []
Side2 = []
for x,y in zip(X2,Y2):
  if SideOfLine( A, B, (x,y)) >= 0:
    Side1.append( (x[0],y[0]) )
  else:
    Side2.append( (x[0],y[0]) )

Side1x = [i[0] for i in Side1]
Side1y = [i[1] for i in Side1]
Side2x = [i[0] for i in Side2]
Side2y = [i[1] for i in Side2]

plt.figure(figsize=(3*R*shape[0], 3*R*shape[1]))
plt.scatter(Side2x, Side2y, s=3, color='b', label=r'd $<$ 0: '+str(len(Side2)))
plt.scatter(Side1x, Side1y, s=3, color='r', label=r'd $\geq$ 0: '+str(len(Side1)))
plt.xlabel('x')
plt.ylabel('y')
#plt.plot(linex, liney, label=linestring)
#plt.scatter(X0cm, Y0cm, color='k', label=comstring)
plt.legend()
plt.savefig('Bisected.png')
plt.show(block=False)









### Center of mass of origins
X0, Y0 = [i.x0 for i in Circles], [i.y0 for i in Circles]
X0cm = sum(X0)/len(X0)
Y0cm = sum(Y0)/len(Y0)
comstring = 'CoM: ('+str(X0cm)+', '+str(Y0cm)+')'

### Using centers of Circles (not randomly-generated points), find theta
X02 = np.reshape(np.array(X0),(-1,1))
ones = np.ones((len(X0),1))
X03 = np.concatenate( (ones,X02), axis=1 )
Y02 = np.reshape(np.array(Y0),(-1,1))

theta0 = np.linalg.inv(X03.T.dot(X03)).dot(X03.T).dot(Y02)
b0, m0 = theta0
line0string = 'y = '+str(round(m0[0],2))+'x + '+str(round(b0[0],2))
line0x = linex[:]
line0y = [np.dot((1,i),theta0)[0] for i in linex]


### Plot of Circles with just centers; no randomly-generated points
# Getting lims of previous plot so below plot will be at same axis ranges
xlim1, xlim2 = plt.xlim()
ylim1, ylim2 = plt.ylim()
plt.figure(figsize=(3*R*shape[0], 3*R*shape[1]))
for i in range(len(Circles)):
  ax = plt.gca()
  circle = plt.Circle(Circles[i].O, Circles[i].radius, fill=False)
  ax.add_artist(circle)
plt.plot(line0x, line0y, label=line0string)
plt.scatter(X02, Y02, color='k', s=3)
plt.scatter(X0cm, Y0cm, color='k', label=comstring)
plt.xlim(xlim1, xlim2)
plt.ylim(ylim1, ylim2)
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.savefig('Fit0.png')
plt.show(block=False)







### Instead of fitting (which seems to skew towards line of three circles,
# do binary search just like NeapolitanAreas?





'''
# Draw randomy line
minx, miny = (0, 0)
maxx, maxy = max([i.x0 for i in Circles])+R, max([i.y0 for i in Circles])+R
plt.legend()
plt.show(block=False)

### Center of mass of origins
X0, Y0 = [i.x0 for i in Circles], [i.y0 for i in Circles]
X0cm = sum(X0)/len(X0)
Y0cm = sum(Y0)/len(Y0)

# http://stackoverflow.com/questions/21565994/method-to-return-the-equation-of-a-straight-line-given-two-points
points = [(minx, miny), (X0cm, Y0cm)]
x_coords, y_coords = zip(*points)
A = np.vstack([x_coords, np.ones(len(x_coords))]).T
m, b = np.linalg.lstsq(A, y_coords)[0]
print("Line Solution is y = {m}x + {b}".format(m=m,b=b))
plt.plot( [minx, (maxx-R)], [b, m*(maxx-R)+b] )
plt.show(block=False)

### Residuals:
X = np.array(X).flatten()
X = np.reshape(X, (-1,1))
Y = np.array(Y).flatten()
Y = np.reshape(Y, (-1,1))
Residuals = [y-(m*x+b) for y,x in zip(Y,X)]
Ressum = np.sum(Residuals)
Resmean = np.mean(Residuals)

YY = m*X+b
# https://en.wikipedia.org/wiki/Mean_squared_error
MSE = np.dot((Y-YY).T, (Y-YY))/len(Y) #sum([(y-(m*x+b))**2 for y,x in zip(Y,X)])/len(Y)

#plt.scatter(X,Residuals)
#plt.show(block=False)
'''
