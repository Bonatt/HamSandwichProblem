
### Inspired by Numberphile video: https://www.youtube.com/watch?v=YCXmUi56rao
# See end of video for a follow-up question: cut the below in half with one cut:
#   oo
#   ooo
# (but he two lines of text are touching)

import numpy as np
import matplotlib.pyplot as plt


### Methods:
# 1. Generate random points on disks and find center of mass.
# 2. Generate random points on rings and find center of mass.
# 3. Just use the five origins and find center of mass.


### Define five circles parameters
# Radius
R = 1
# Centers
#   45
#   123
'''
Centers = [(R,R)]
Center += [ (Centers[0] ]
'''
'''
circle1_0 = (R,R)
circle2_0 = (circle1_0[0]+2*R
circle3_0 = (0,0)
circle4_0 = 
circle5_0 = (0,0)
'''
class Circle():
  def __init__(self, x0, y0, radius):
    self.x0 = x0
    self.y0 = y0
    self.O = (x0, y0)
    self.radius = radius


### Fill circle/distk uniformly with n points
def Fill(x0, y0, R=R, n=1000):
  # n = int
  # O = (float,float)
  #x0, y0 = O
  r = np.random.uniform(low=0., high=R, size=n)             # radius of point
  theta = np.random.uniform(low=0., high=2.*np.pi, size=n)  # angle of point

  # Location of points in rectangular coords.
  x = x0 + np.sqrt(r)*np.cos(theta)
  y = y0 + np.sqrt(r)*np.sin(theta)
  return x, y

'''
x,y = Fill(Circles[0].O,n=100)

plt.figure()
plt.plot(x,y, label='0')
plt.legend()
plt.show(block=False)
'''

### Make Circle objects. Make in shape of
#   ooo
#   ooo
# then del top-right.

# Number of circles in problem. One to be del'd.
nCircles = 5+1

# Number of randomly-generated points
n = 1000



### I'm sure this could be simply cast/vectorized somehow...
Circles = []
shape = (3,2) # (x,y)
#for i in range(nCircles):
 
# 1, 3
for y in range(shape[1]):
  Y = R * (2*y + 1)

  # 1, 3, (5)
  for x in range(shape[0]):
    X = R * (2*x + 1)
      
    Circles.append( Circle(X,Y,R) )
    
print('\n(x, y)')
for i in Circles: print(i.O)
#Circles = np.reshape(Circles, shape)
del Circles[-1]
print('\n(x, y)')
for i in Circles: print(i.O)


'''
#x = [R*(2*i+1) for i in range(nCircles//shape[1])]
#Y = [R*(2*i+1) for i in range(nCircles//shape[0])]

X = [R*(2*i+1)%(R*2*nCircles//shape[1]) for i in range(nCircles)]
Y = [R*(2*i+1)%(R*2*nCircles//shape[0]) for i in range(nCircles)]
[(x,y) for x,y in zip(X,Y)]
'''

# Lists to hold all randomly-generated points
X = []
Y = []

### Plot Circles: points and borders
plt.figure(figsize=(3*R*shape[0], 3*R*shape[1]))
for i in range(len(Circles)):

  # Plot Circle border
  # https://stackoverflow.com/questions/9215658/plot-a-circle-with-pyplot/23498706
  ax = plt.gca() # get current axes
  circle = plt.Circle(Circles[i].O, Circles[i].radius, fill=False)
  ax.add_artist(circle)

  # Fill Circle with points
  x,y = Fill(Circles[i].x0, Circles[i].y0, R, n)
  X.append(x)
  Y.append(y)
  # Create scatter in Circle
  plt.scatter(x, y, s=3, label='Circle '+str(i+1))
  plt.text(Circles[i].x0, Circles[i].y0, str(i+1))

# Draw randomy line
minx, miny = (0, 0)
maxx, maxy = max([i.x0 for i in Circles])+R, max([i.y0 for i in Circles])+R
'''
randx = np.random.uniform(low=minx, high=maxx)
randy = np.random.uniform(low=miny, high=maxy)
plt.plot(randx, randy)
'''
'''
step = 0.1
for y in np.arange(miny, maxy, step):
  for x in np.arange(minx, maxx, step):
    # y = m*x + b
'''

plt.legend()
plt.show(block=False)


'''
from sklearn import linear_model
# Flatten X,Y
X = np.array(X).flatten()
X = np.reshape(X, (-1,1))
Y = np.array(Y).flatten()
Y = np.reshape(Y, (-1,1))
from sklearn import linear_model
regr = linear_model.LinearRegression()
regr.fit(X,Y)

plt.plot(0, 100*regr.coef_)
plt.show()
'''





### Center of mass of origins
X0, Y0 = [i.x0 for i in Circles], [i.y0 for i in Circles]

Xcm = sum(X0)/len(X0)
Ycm = sum(Y0)/len(Y0)

#plt.plot([0,Xcm], [0,Ycm])



# http://stackoverflow.com/questions/21565994/method-to-return-the-equation-of-a-straight-line-given-two-points
#from numpy import ones,vstack
#from numpy.linalg import lstsq
points = [(minx, miny), (Xcm, Ycm)]
x_coords, y_coords = zip(*points)
A = np.vstack([x_coords, np.ones(len(x_coords))]).T
m, b = np.linalg.lstsq(A, y_coords)[0]
print("Line Solution is y = {m}x + {b}".format(m=m,b=b))

plt.plot( [minx, (maxx-R)], [b, m*(maxx-R)+b] )
plt.show(block=False)




### Residuals:
X = np.array(X).flatten()
X = np.reshape(X, (-1,1))
Y = np.array(Y).flatten()
Y = np.reshape(Y, (-1,1))
Residuals = [y-(m*x+b) for y,x in zip(Y,X)]
Ressum = np.sum(Residuals)
Resmean = np.mean(Residuals)

YY = m*X+b
# https://en.wikipedia.org/wiki/Mean_squared_error
MSE = np.dot((Y-YY).T, (Y-YY))/len(Y) #sum([(y-(m*x+b))**2 for y,x in zip(Y,X)])/len(Y)

# https://en.wikipedia.org/wiki/Root_mean_square
#RMS = 

#plt.scatter(X,Residuals)
#plt.show(block=False)


#np.linalg.inv(X.T.dot(X)).dot(X.T).dot(Y)
#np.ones((len(X),1))
