### Ham sandwich problem, kind of

Inspired by [Numberphile video on Youtube](https://www.youtube.com/watch?v=YCXmUi56rao). 
See end of video for a follow-up question that I attempt to answer here via Monte Carlo.
Cut in to two equal parts with one straight cut:
![Prompt](Prompt.png)

__Caveat: I have not yet watched the end of the above video so I do not know the correct solution__

I generated five circles of unitary radius and plotted them according to the arrangement given. 
I fit all five center points with a straight line. I also calculated the "Center of Mass" of all five center points.

![Fit0](Fit0.png)

I also uniformly-generated _n_ = 1000 points within each circle. I did the same as above but with all 5000 points instead.

![Fit](Fit.png)

I deteremined if a point was on one side of the line, or the other, and plotted that below.

![Bisected](Bisected.png)

But it turns out that this method is incorrect. There are more red points than blue points. 
This is not a function of _n_ either as I get very similer result (*10) for _n_ = 10000.
Interesting, even though this method is incorrect (probably because the fit is skewed by the "outlier" circle at (5,1), as expected),
the CoM still sits perfectly on the line of best fit. I suppose this should make sense:
a plane through the center of mass does not necessarily divide an object in half (by area/volume nor by mass). 
But it was all I thought of in the hour or so I attempted this problem.

I can think of other methods but have not pursued them yet. I can also analytically/logically think of where several cuts
could go (all horizontal or all vertical), so I do know there are are least two solutions to this problem (maybe infinite).
